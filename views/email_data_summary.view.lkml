view: email_data_summary {
  sql_table_name: klaviyo_worldpantry.email_data_summary ;;

  dimension: ccn {
    type: number
    sql: ${TABLE}.ccn ;;
  }

  dimension: click_rate {
    type: number
    sql: ${TABLE}.click_rate ;;
  }

  dimension: contact_loss {
    type: number
    sql: ${TABLE}.contact_loss ;;
  }

  dimension: email_name {
    type: string
    sql: ${TABLE}.email_name ;;
  }

  dimension: opens {
    type: number
    sql: ${TABLE}.opens ;;
  }

  dimension: orders {
    type: number
    sql: ${TABLE}.orders ;;
  }

  dimension: revenue {
    type: number
    sql: ${TABLE}.revenue ;;
  }

  dimension_group: senddate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.senddate ;;
  }

  dimension: sent {
    type: number
    sql: ${TABLE}.sent ;;
  }

  measure: count {
    type: count
    drill_fields: [email_name]
  }
}
