view: event {
  sql_table_name: klaviyo_worldpantry.event ;;
  drill_fields: [property_event_id]

  dimension: property_event_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.property_event_id ;;
  }

  dimension: _fivetran_deleted {
    type: yesno
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: _variation {
    type: string
    sql: ${TABLE}._variation ;;
  }

  dimension: campaign_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.campaign_id ;;
  }

  dimension: ccn {
    type: number
    sql: ${TABLE}.ccn ;;
  }

  dimension_group: datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.datetime ;;
  }

  dimension: flow_id {
    type: string
    sql: ${TABLE}.flow_id ;;
  }

  dimension: flow_message_id {
    type: string
    sql: ${TABLE}.flow_message_id ;;
  }

  dimension: id {
    type: string
    sql: ${TABLE}.id ;;
  }

  dimension: metric_id {
    type: string
    sql: ${TABLE}.metric_id ;;
  }

  dimension: person_id {
    type: string
    sql: ${TABLE}.person_id ;;
  }

  dimension: property_attribute_charge_interval_frequency {
    type: number
    sql: ${TABLE}.property_attribute_charge_interval_frequency ;;
  }

  dimension: property_attribute_charge_interval_unit_type {
    type: string
    sql: ${TABLE}.property_attribute_charge_interval_unit_type ;;
  }

  dimension: property_attribute_charge_on_day_of_month {
    type: number
    sql: ${TABLE}.property_attribute_charge_on_day_of_month ;;
  }

  dimension: property_attribute_discounted_price {
    type: string
    sql: ${TABLE}.property_attribute_discounted_price ;;
  }

  dimension: property_attribute_frequency_num {
    type: number
    sql: ${TABLE}.property_attribute_frequency_num ;;
  }

  dimension: property_attribute_frequency_type {
    type: number
    sql: ${TABLE}.property_attribute_frequency_type ;;
  }

  dimension: property_attribute_frequency_type_text {
    type: string
    sql: ${TABLE}.property_attribute_frequency_type_text ;;
  }

  dimension: property_attribute_group_id {
    type: number
    sql: ${TABLE}.property_attribute_group_id ;;
  }

  dimension: property_attribute_recurring_order {
    type: string
    sql: ${TABLE}.property_attribute_recurring_order ;;
  }

  dimension: property_attribute_ro_discount_percentage {
    type: number
    sql: ${TABLE}.property_attribute_ro_discount_percentage ;;
  }

  dimension: property_attribute_ro_unformatted_price {
    type: number
    sql: ${TABLE}.property_attribute_ro_unformatted_price ;;
  }

  dimension: property_attribute_shipping_interval_frequency {
    type: number
    sql: ${TABLE}.property_attribute_shipping_interval_frequency ;;
  }

  dimension: property_attribute_shipping_interval_unit_type {
    type: string
    sql: ${TABLE}.property_attribute_shipping_interval_unit_type ;;
  }

  dimension: property_attribute_subscription_id {
    type: number
    sql: ${TABLE}.property_attribute_subscription_id ;;
  }

  dimension: property_attribution {
    type: string
    sql: ${TABLE}.property_attribution ;;
  }

  dimension: property_bounce_type {
    type: string
    sql: ${TABLE}.property_bounce_type ;;
  }

  dimension: property_brand {
    type: string
    sql: ${TABLE}.property_brand ;;
  }

  dimension: property_browser {
    type: string
    sql: ${TABLE}.property_browser ;;
  }

  dimension: property_campaign_name {
    type: string
    sql: ${TABLE}.property_campaign_name ;;
  }

  dimension: property_cancellation_reason {
    type: string
    sql: ${TABLE}.property_cancellation_reason ;;
  }

  dimension: property_categories {
    type: string
    sql: ${TABLE}.property_categories ;;
  }

  dimension: property_client_canonical {
    type: string
    sql: ${TABLE}.property_client_canonical ;;
  }

  dimension: property_client_name {
    type: string
    sql: ${TABLE}.property_client_name ;;
  }

  dimension: property_client_os {
    type: string
    sql: ${TABLE}.property_client_os ;;
  }

  dimension: property_client_os_family {
    type: string
    sql: ${TABLE}.property_client_os_family ;;
  }

  dimension: property_client_type {
    type: string
    sql: ${TABLE}.property_client_type ;;
  }

  dimension: property_cohort_message_send_cohort {
    type: string
    sql: ${TABLE}.property_cohort_message_send_cohort ;;
  }

  dimension: property_cohort_variation_send_cohort {
    type: string
    sql: ${TABLE}.property_cohort_variation_send_cohort ;;
  }

  dimension: property_collections {
    type: string
    sql: ${TABLE}.property_collections ;;
  }

  dimension: property_compare_at_price {
    type: string
    sql: ${TABLE}.property_compare_at_price ;;
  }

  dimension: property_currency_code {
    type: string
    sql: ${TABLE}.property_currency_code ;;
  }

  dimension: property_discount_codes {
    type: string
    sql: ${TABLE}.property_discount_codes ;;
  }

  dimension: property_email_domain {
    type: string
    sql: ${TABLE}.property_email_domain ;;
  }

  dimension: property_esp {
    type: number
    sql: ${TABLE}.property_esp ;;
  }

  dimension: property_extra {
    type: string
    sql: ${TABLE}.property_extra ;;
  }

  dimension: property_fulfillment_hours {
    type: number
    sql: ${TABLE}.property_fulfillment_hours ;;
  }

  dimension: property_fulfillment_status {
    type: string
    sql: ${TABLE}.property_fulfillment_status ;;
  }

  dimension: property_has_partial_fulfillments {
    type: yesno
    sql: ${TABLE}.property_has_partial_fulfillments ;;
  }

  dimension: property_image_url {
    type: string
    sql: ${TABLE}.property_image_url ;;
  }

  dimension: property_internal {
    type: string
    sql: ${TABLE}.property_internal ;;
  }

  dimension: property_is_session_activity {
    type: yesno
    sql: ${TABLE}.property_is_session_activity ;;
  }

  dimension: property_item {
    type: string
    sql: ${TABLE}.property_item ;;
  }

  dimension: property_item_count {
    type: number
    sql: ${TABLE}.property_item_count ;;
  }

  dimension: property_item_names {
    type: string
    sql: ${TABLE}.property_item_names ;;
  }

  dimension: property_items {
    type: string
    sql: ${TABLE}.property_items ;;
  }

  dimension: property_list {
    type: string
    sql: ${TABLE}.property_list ;;
  }

  dimension: property_message_interaction {
    type: string
    sql: ${TABLE}.property_message_interaction ;;
  }

  dimension: property_name {
    type: string
    sql: ${TABLE}.property_name ;;
  }

  dimension: property_order_interval_days {
    type: number
    sql: ${TABLE}.property_order_interval_days ;;
  }

  dimension: property_os {
    type: string
    sql: ${TABLE}.property_os ;;
  }

  dimension: property_page {
    type: string
    sql: ${TABLE}.property_page ;;
  }

  dimension: property_platform {
    type: string
    sql: ${TABLE}.property_platform ;;
  }

  dimension: property_price {
    type: string
    sql: ${TABLE}.property_price ;;
  }

  dimension: property_product_id {
    type: number
    sql: ${TABLE}.property_product_id ;;
  }

  dimension: property_product_name {
    type: string
    sql: ${TABLE}.property_product_name ;;
  }

  dimension: property_promo_id {
    type: number
    sql: ${TABLE}.property_promo_id ;;
  }

  dimension: property_promo_title {
    type: string
    sql: ${TABLE}.property_promo_title ;;
  }

  dimension: property_quantity {
    type: number
    sql: ${TABLE}.property_quantity ;;
  }

  dimension: property_session_end {
    type: number
    sql: ${TABLE}.property_session_end ;;
  }

  dimension: property_shipping_rate {
    type: string
    sql: ${TABLE}.property_shipping_rate ;;
  }

  dimension: property_sku {
    type: string
    sql: ${TABLE}.property_sku ;;
  }

  dimension: property_source_name {
    type: string
    sql: ${TABLE}.property_source_name ;;
  }

  dimension: property_status {
    type: string
    sql: ${TABLE}.property_status ;;
  }

  dimension: property_subject {
    type: string
    sql: ${TABLE}.property_subject ;;
  }

  dimension: property_tags {
    type: string
    sql: ${TABLE}.property_tags ;;
  }

  dimension: property_total_discounts {
    type: number
    sql: ${TABLE}.property_total_discounts ;;
  }

  dimension: property_url {
    type: string
    sql: ${TABLE}.property_url ;;
  }

  dimension: property_value {
    type: number
    sql: ${TABLE}.property_value ;;
  }

  dimension: property_variant {
    type: string
    sql: ${TABLE}.property_variant ;;
  }

  dimension: property_variant_id {
    type: number
    sql: ${TABLE}.property_variant_id ;;
  }

  dimension: property_variant_name {
    type: string
    sql: ${TABLE}.property_variant_name ;;
  }

  dimension: property_variant_option_color {
    type: string
    sql: ${TABLE}.property_variant_option_color ;;
  }

  dimension: property_variant_option_first_flavor {
    type: string
    sql: ${TABLE}.property_variant_option_first_flavor ;;
  }

  dimension: property_variant_option_flavor {
    type: string
    sql: ${TABLE}.property_variant_option_flavor ;;
  }

  dimension: property_variant_option_pack_size {
    type: string
    sql: ${TABLE}.property_variant_option_pack_size ;;
  }

  dimension: property_variant_option_second_flavor {
    type: string
    sql: ${TABLE}.property_variant_option_second_flavor ;;
  }

  dimension: property_variant_option_size {
    type: string
    sql: ${TABLE}.property_variant_option_size ;;
  }

  dimension: property_variant_option_title {
    type: string
    sql: ${TABLE}.property_variant_option_title ;;
  }

  dimension: property_vendor {
    type: string
    sql: ${TABLE}.property_vendor ;;
  }

  dimension_group: timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.timestamp ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      property_event_id,
      property_source_name,
      property_name,
      property_variant_name,
      property_campaign_name,
      property_client_name,
      property_product_name,
      campaign.id,
      campaign.from_name,
      campaign.name
    ]
  }
}
