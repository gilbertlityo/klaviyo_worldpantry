connection: "worldpantry-redshift"

# include all the views
include: "/views/**/*.view"

datagroup: klaviyo_worldpantry_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: klaviyo_worldpantry_default_datagroup

explore: campaign {}

explore: campaign_summary {
  join: campaign {
    type: left_outer
    sql_on: ${campaign_summary.campaign_id} = ${campaign.id} ;;
    relationship: many_to_one
  }
}

explore: email_data_summary {}

explore: event {
  join: campaign {
    type: left_outer
    sql_on: ${event.campaign_id} = ${campaign.id} ;;
    relationship: many_to_one
  }
}
